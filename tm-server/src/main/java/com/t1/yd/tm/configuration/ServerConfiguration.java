package com.t1.yd.tm.configuration;

import com.t1.yd.tm.api.service.IPropertyService;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("com.t1.yd.tm")
@EnableTransactionManagement
public class ServerConfiguration {

    @Bean
    @NotNull
    public DataSource dataSource(@Autowired @NotNull final IPropertyService propertyService) {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDbDriver());
        dataSource.setUrl(propertyService.getDbServer());
        dataSource.setUsername(propertyService.getDbUsername());
        dataSource.setPassword(propertyService.getDbPassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(@Autowired @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean getEntityManagerFactory(@Autowired @NotNull final IPropertyService propertyService,
                                                                          @Autowired @NotNull final DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan("com.t1.yd.tm.model", "com.t1.yd.tm.dto.model");

        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDbDriver());
        settings.put(Environment.URL, propertyService.getDbServer());
        settings.put(Environment.USER, propertyService.getDbUsername());
        settings.put(Environment.PASS, propertyService.getDbPassword());
        settings.put(Environment.DIALECT, propertyService.getDbSqlDialect());
        settings.put(Environment.SHOW_SQL, propertyService.getDbShowSqlFlg());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDbStrategy());

        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDbCacheUseSecondLevel());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getDbCacheUseQuery());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getDbCacheUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getDbCacheRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDbCacheProviderConfig());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getDbCacheFactoryClass());

        entityManagerFactoryBean.setJpaPropertyMap(settings);

        return entityManagerFactoryBean;
    }

}
