package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.repository.dto.IDtoUserOwnedRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.dto.IUserOwnedDtoService;
import com.t1.yd.tm.dto.model.AbstractUserOwnedEntityDTO;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.entity.EntityNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedDtoService<E extends AbstractUserOwnedEntityDTO, R extends IDtoUserOwnedRepository<E>> extends AbstractDtoService<E, R> implements IUserOwnedDtoService<E> {

    @Autowired
    public AbstractUserOwnedDtoService(@NotNull final ILoggerService loggerService) {
        super(loggerService);
    }

    @Override
    @SneakyThrows
    public E add(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        getRepository().add(entity);
        return entity;
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        getRepository().clear(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        return getRepository().findAll(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        if (comparator == null) return findAll(userId);
        return getRepository().findAll(userId, comparator);
    }

    @NotNull
    public List<E> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return getRepository().findOneById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        return getRepository().findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final E result = findOneById(userId, id);
        if (result == null) throw new EntityNotFoundException();
        getRepository().removeById(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        @Nullable final E result = findOneByIndex(userId, index);
        if (result == null) throw new EntityNotFoundException();
        getRepository().removeByIndex(userId, index);
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return getRepository().findOneById(userId, id) != null;
    }

    @Override
    @SneakyThrows
    public int getSize(@NotNull final String userId) {
        return getRepository().findAll(userId).size();
    }

}
