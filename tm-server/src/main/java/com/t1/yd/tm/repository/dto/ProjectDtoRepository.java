package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.api.repository.dto.IDtoProjectRepository;
import com.t1.yd.tm.dto.model.ProjectDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class ProjectDtoRepository extends AbstractDtoUserOwnedRepository<ProjectDTO> implements IDtoProjectRepository {

    @Override
    protected @NotNull String getEntityName() {
        return getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<ProjectDTO> getClazz() {
        return ProjectDTO.class;
    }

}
