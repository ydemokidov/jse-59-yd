package com.t1.yd.tm.service.model;

import com.t1.yd.tm.api.repository.model.ITaskRepository;
import com.t1.yd.tm.api.service.ILoggerService;
import com.t1.yd.tm.api.service.model.ITaskService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    private final ITaskRepository repository;

    @Autowired
    public TaskService(@NotNull final ILoggerService loggerService,
                       @NotNull final ITaskRepository repository) {
        super(loggerService);
        this.repository = repository;
    }

    @Override
    @Transactional
    public @NotNull Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @Override
    @Transactional
    public @NotNull Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return update(task);
    }

    @Override
    @Transactional
    public @NotNull Task changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return update(task);
    }

    @Override
    public @NotNull List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return repository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository() {
        return repository;
    }

    @Override
    @Transactional
    public Task add(@NotNull String userId, @NotNull Task entity) {
        return super.add(userId, entity);
    }

    @Override
    @Transactional
    public void clear(@NotNull String userId) {
        super.clear(userId);
    }

    @Nullable
    @Override
    @Transactional
    public Task removeById(@NotNull String userId, @NotNull String id) {
        return super.removeById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public Task removeByIndex(@NotNull String userId, @NotNull Integer index) {
        return super.removeByIndex(userId, index);
    }
}
