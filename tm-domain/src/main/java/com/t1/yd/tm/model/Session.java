package com.t1.yd.tm.model;

import com.t1.yd.tm.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "sessions")
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Session extends AbstractUserOwnedEntity {

    @NotNull
    @Column(nullable = false)
    private Date date = new Date();

    @Nullable
    @Column(length = 50)
    @Enumerated(EnumType.STRING)
    private Role role = null;

    public Session(@NotNull final User user) {
        super(user);
    }

}