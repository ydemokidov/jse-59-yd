package com.t1.yd.tm.dto.response.data;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import org.jetbrains.annotations.Nullable;

public class DataFasterXmlXmlLoadResponse extends AbstractResultResponse {

    public DataFasterXmlXmlLoadResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public DataFasterXmlXmlLoadResponse() {

    }
}
