package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import org.jetbrains.annotations.Nullable;

public class UserLogoutResponse extends AbstractResultResponse {

    public UserLogoutResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public UserLogoutResponse() {
        super();
    }

}
