package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.dto.model.ProjectDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class ProjectRemoveByIndexResponse extends AbstractProjectResponse {

    public ProjectRemoveByIndexResponse(@Nullable ProjectDTO projectDTO) {
        super(projectDTO);
    }

    public ProjectRemoveByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
