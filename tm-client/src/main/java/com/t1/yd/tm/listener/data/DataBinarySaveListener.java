package com.t1.yd.tm.listener.data;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.data.DataBinarySaveRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.event.ConsoleEvent;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class DataBinarySaveListener extends AbstractDataListener {

    @NotNull
    private final String name = "save_binary";
    @NotNull
    private final String description = "Save data to binary file";

    @Autowired
    public DataBinarySaveListener(@NotNull final ITokenService tokenService,
                                  @NotNull final IDataEndpoint dataEndpointClient) {
        super(tokenService, dataEndpointClient);
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinarySaveListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[BINARY SAVE DATA]");
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest();
        request.setToken(getToken());
        getDataEndpointClient().binarySave(request);
        System.out.println("[DATA SAVED]");
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
