package com.t1.yd.tm.listener.task;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.task.TaskRemoveByIndexRequest;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class TaskRemoveByIndexListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task_remove_by_id";
    @NotNull
    public static final String DESCRIPTION = "Remove task by Id";

    @Autowired
    public TaskRemoveByIndexListener(@NotNull final ITokenService tokenService,
                                     @NotNull final ITaskEndpoint taskEndpointClient) {
        super(tokenService, taskEndpointClient);
    }

    @Override
    @EventListener(condition = "@taskRemoveByIndexListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest();
        request.setIndex(index);
        request.setToken(getToken());
        getTaskEndpointClient().removeTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
