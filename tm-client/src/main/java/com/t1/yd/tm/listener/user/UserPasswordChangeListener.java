package com.t1.yd.tm.listener.user;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.user.UserPasswordChangeRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class UserPasswordChangeListener extends AbstractUserListener {

    @NotNull
    private final String name = "user_password_change";
    @NotNull
    private final String description = "Change user password";

    @Autowired
    public UserPasswordChangeListener(@NotNull final ITokenService tokenService,
                                      @NotNull final IUserEndpoint userEndpointClient,
                                      @NotNull final IAuthEndpoint authEndpointClient) {
        super(tokenService, userEndpointClient, authEndpointClient);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@userPasswordChangeListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String newPwd = TerminalUtil.nextLine();
        @NotNull final UserPasswordChangeRequest request = new UserPasswordChangeRequest();
        request.setNewPassword(newPwd);
        request.setToken(getToken());
        getUserEndpoint().passwordChange(request);
        System.out.println("[PASSWORD CHANGED]");
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}
