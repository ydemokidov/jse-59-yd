package com.t1.yd.tm.listener.data;

import com.t1.yd.tm.api.endpoint.IDataEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.data.DataBackupLoadRequest;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.event.ConsoleEvent;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class DataBackupLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "load_backup";
    @NotNull
    private final String description = "Load data from backup file";

    @Autowired
    public DataBackupLoadListener(@NotNull final ITokenService tokenService,
                                  @NotNull final IDataEndpoint dataEndpointClient) {
        super(tokenService, dataEndpointClient);
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBackupLoadListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[LOAD BACKUP DATA]");
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest();
        request.setToken(getToken());
        getDataEndpointClient().backupLoad(request);
        System.out.println("[BACKUP DATA LOADED]");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
