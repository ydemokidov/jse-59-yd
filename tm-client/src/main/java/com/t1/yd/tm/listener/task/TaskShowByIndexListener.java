package com.t1.yd.tm.listener.task;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.dto.request.task.TaskShowByIndexRequest;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class TaskShowByIndexListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task_show_by_index";
    @NotNull
    public static final String DESCRIPTION = "Show task by Index";

    @Autowired
    public TaskShowByIndexListener(@NotNull final ITokenService tokenService,
                                   @NotNull final ITaskEndpoint taskEndpointClient) {
        super(tokenService, taskEndpointClient);
    }

    @Override
    @EventListener(condition = "@taskShowByIndexListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER INDEX:");

        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest();
        request.setIndex(index);
        request.setToken(getToken());

        @Nullable final TaskDTO taskDTO = getTaskEndpointClient().showTaskByIndex(request).getTaskDTO();
        showTask(taskDTO);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
