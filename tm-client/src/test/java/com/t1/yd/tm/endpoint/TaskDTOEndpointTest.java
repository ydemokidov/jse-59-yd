package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.dto.request.project.ProjectCreateRequest;
import com.t1.yd.tm.dto.request.task.*;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.marker.IntegrationCategory;
import com.t1.yd.tm.service.PropertyService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;

import java.util.List;

import static com.t1.yd.tm.constant.EndpointTestData.ADMIN_LOGIN;
import static com.t1.yd.tm.constant.EndpointTestData.ADMIN_PASSWORD;

@Category(IntegrationCategory.class)
public class TaskDTOEndpointTest extends AbstractEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Nullable
    private String token;
    @Nullable
    private TaskDTO testTaskDTO;

    @NotNull
    private TaskDTO createTask() {
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest();
        createRequest.setToken(token);
        createRequest.setName("TASK1");
        createRequest.setDescription("Description");
        @Nullable final TaskDTO taskDTOCreated = taskEndpoint.createTask(createRequest).getTaskDTO();
        Assert.assertNotNull(taskDTOCreated);
        return taskDTOCreated;
    }

    @NotNull
    private ProjectDTO createProject() {
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest();
        createRequest.setToken(token);
        createRequest.setName("PROJECT1");
        createRequest.setDescription("Description");
        @Nullable final ProjectDTO projectDTOCreated = projectEndpoint.createProject(createRequest).getProjectDTO();
        Assert.assertNotNull(projectDTOCreated);
        return projectDTOCreated;
    }

    @Nullable
    private TaskDTO findTaskById(final String Id) {
        @NotNull final TaskShowByIdRequest showByIdRequest = new TaskShowByIdRequest();
        showByIdRequest.setToken(token);
        showByIdRequest.setId(Id);
        return taskEndpoint.showTaskById(showByIdRequest).getTaskDTO();
    }

    @Nullable
    private TaskDTO findTaskByIndex(final Integer Index) {
        @NotNull final TaskShowByIndexRequest showByIndexRequest = new TaskShowByIndexRequest();
        showByIndexRequest.setToken(token);
        showByIndexRequest.setIndex(Index);
        return taskEndpoint.showTaskByIndex(showByIndexRequest).getTaskDTO();
    }

    @Before
    public void before() {
        token = login(ADMIN_LOGIN, ADMIN_PASSWORD);
        saveBackup(token);
        testTaskDTO = createTask();
    }

    @After
    public void after() {
        loadBackup(token);
        logout(token);
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest();
        request.setToken(token);
        request.setId(testTaskDTO.getId());
        request.setStatus(Status.COMPLETED.toString());
        taskEndpoint.changeTaskStatusById(request);
        @Nullable TaskDTO taskDTOFound = findTaskById(testTaskDTO.getId());
        Assert.assertEquals(taskDTOFound.getStatus(), Status.COMPLETED);
    }

    @Test
    public void changeTaskStatusByIndex() {
        @NotNull final Status statusBefore = findTaskByIndex(0).getStatus();
        @NotNull final Status statusToSet = statusBefore == Status.COMPLETED ? Status.NOT_STARTED : Status.COMPLETED;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        request.setStatus(statusToSet.toString());
        taskEndpoint.changeTaskStatusByIndex(request);
        @Nullable TaskDTO taskDTOFound = findTaskByIndex(0);
        Assert.assertEquals(taskDTOFound.getStatus(), statusToSet);
    }

    @Test
    public void clearTasks() {
        @NotNull final TaskClearRequest request = new TaskClearRequest();
        request.setToken(token);
        taskEndpoint.clearTasks(request);
        TaskNotFoundException taskNotFoundException = new TaskNotFoundException();
        thrown.expectMessage(taskNotFoundException.getMessage());
        Assert.assertNull(findTaskById(testTaskDTO.getId()));
    }

    @Test
    public void completeTaskById() {
        @NotNull final TaskChangeStatusByIdRequest changeRequest = new TaskChangeStatusByIdRequest();
        changeRequest.setToken(token);
        changeRequest.setId(testTaskDTO.getId());
        changeRequest.setStatus(Status.IN_PROGRESS.toString());
        taskEndpoint.changeTaskStatusById(changeRequest);
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest();
        request.setToken(token);
        request.setId(testTaskDTO.getId());
        taskEndpoint.completeTaskById(request);
        @Nullable final TaskDTO taskDTOFound = findTaskById(testTaskDTO.getId());
        Assert.assertEquals(taskDTOFound.getStatus(), Status.COMPLETED);
    }

    @Test
    public void completeTaskByIndex() {
        @NotNull final TaskChangeStatusByIndexRequest changeRequest = new TaskChangeStatusByIndexRequest();
        changeRequest.setToken(token);
        changeRequest.setIndex(0);
        changeRequest.setStatus(Status.IN_PROGRESS.toString());
        taskEndpoint.changeTaskStatusByIndex(changeRequest);
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        taskEndpoint.completeTaskByIndex(request);
        @Nullable final TaskDTO taskDTOFound = findTaskByIndex(0);
        Assert.assertEquals(taskDTOFound.getStatus(), Status.COMPLETED);
    }

    @Test
    public void listTasks() {
        @NotNull final TaskListRequest request = new TaskListRequest();
        request.setToken(token);
        request.setSort(Sort.BY_NAME.toString());
        @Nullable final List<TaskDTO> taskDTOS = taskEndpoint.listTasks(request).getTaskDTOS();
        Assert.assertTrue(taskDTOS.size() >= 1);
    }

    @Test
    public void removeTaskById() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest();
        request.setToken(token);
        request.setId(testTaskDTO.getId());
        taskEndpoint.removeTaskById(request);
        TaskNotFoundException taskNotFoundException = new TaskNotFoundException();
        thrown.expectMessage(taskNotFoundException.getMessage());
        Assert.assertNull(findTaskById(testTaskDTO.getId()));
    }

    @Test
    public void removeTaskByIndex() {
        @NotNull final TaskDTO taskDTOFound = findTaskByIndex(0);
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        taskEndpoint.removeTaskByIndex(request);
        Assert.assertNotEquals(taskDTOFound.getId(), findTaskByIndex(0).getId());
    }

    @Test
    public void startTaskById() {
        @NotNull final TaskChangeStatusByIdRequest changeRequest = new TaskChangeStatusByIdRequest();
        changeRequest.setToken(token);
        changeRequest.setId(testTaskDTO.getId());
        changeRequest.setStatus(Status.NOT_STARTED.toString());
        taskEndpoint.changeTaskStatusById(changeRequest);
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest();
        request.setToken(token);
        request.setId(testTaskDTO.getId());
        taskEndpoint.startTaskById(request);
        @Nullable final TaskDTO taskDTOFound = findTaskById(testTaskDTO.getId());
        Assert.assertEquals(taskDTOFound.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startTaskByIndex() {
        @NotNull final TaskChangeStatusByIndexRequest changeRequest = new TaskChangeStatusByIndexRequest();
        changeRequest.setToken(token);
        changeRequest.setIndex(0);
        changeRequest.setStatus(Status.NOT_STARTED.toString());
        taskEndpoint.changeTaskStatusByIndex(changeRequest);
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        taskEndpoint.startTaskByIndex(request);
        @Nullable final TaskDTO taskDTOFound = findTaskByIndex(0);
        Assert.assertEquals(taskDTOFound.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void updateTaskById() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest();
        request.setToken(token);
        request.setId(testTaskDTO.getId());
        request.setName(testTaskDTO.getName() + "1");
        request.setDescription(testTaskDTO.getDescription() + "1");
        taskEndpoint.updateTaskById(request);
        @Nullable final TaskDTO taskDTOFound = findTaskById(testTaskDTO.getId());
        Assert.assertEquals(taskDTOFound.getName(), testTaskDTO.getName() + "1");
        Assert.assertEquals(taskDTOFound.getDescription(), testTaskDTO.getDescription() + "1");
    }

    @Test
    public void updateTaskByIndex() {
        @Nullable final TaskDTO taskDTOToChange = findTaskByIndex(0);
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        request.setName(taskDTOToChange.getName() + "1");
        request.setDescription(taskDTOToChange.getDescription() + "1");
        taskEndpoint.updateTaskByIndex(request);
        @Nullable final TaskDTO taskDTOFound = findTaskById(taskDTOToChange.getId());
        Assert.assertEquals(taskDTOFound.getName(), taskDTOToChange.getName() + "1");
        Assert.assertEquals(taskDTOFound.getDescription(), taskDTOToChange.getDescription() + "1");
    }

    @Test
    public void bindUnbindTaskToProject() {
        @NotNull final ProjectDTO testProjectDTO = createProject();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest();
        bindRequest.setToken(token);
        bindRequest.setId(testTaskDTO.getId());
        bindRequest.setProjectId(testProjectDTO.getId());
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest();
        request.setToken(token);
        request.setProjectId(testProjectDTO.getId());
        @Nullable List<TaskDTO> taskDTOS = taskEndpoint.listTasksByProjectId(request).getTaskDTOS();
        Assert.assertFalse(taskDTOS.isEmpty());

        @NotNull final TaskUnbindFromProjectRequest unbindRequest = new TaskUnbindFromProjectRequest();
        unbindRequest.setToken(token);
        unbindRequest.setId(testTaskDTO.getId());
        unbindRequest.setProjectId(testProjectDTO.getId());
        taskEndpoint.unbindTaskFromProject(unbindRequest);
        taskDTOS = taskEndpoint.listTasksByProjectId(request).getTaskDTOS();
        Assert.assertNull(taskDTOS);
    }

}