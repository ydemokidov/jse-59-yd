package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.dto.request.project.*;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.marker.IntegrationCategory;
import com.t1.yd.tm.service.PropertyService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;

import java.util.List;

import static com.t1.yd.tm.constant.EndpointTestData.ADMIN_LOGIN;
import static com.t1.yd.tm.constant.EndpointTestData.ADMIN_PASSWORD;

@Category(IntegrationCategory.class)
public class ProjectDTOEndpointTest extends AbstractEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Nullable
    private String token;
    @Nullable
    private ProjectDTO testProjectDTO;

    @Nullable
    private ProjectDTO createProject() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest();
        request.setToken(token);
        request.setName("PROJECT1");
        request.setDescription("Description");
        @Nullable final ProjectDTO projectDTOCreated = projectEndpoint.createProject(request).getProjectDTO();
        return projectDTOCreated;
    }

    @Nullable
    private ProjectDTO findProjectById(final String Id) {
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest();
        request.setToken(token);
        request.setId(Id);
        return projectEndpoint.showProjectById(request).getProjectDTO();
    }

    @Nullable
    private ProjectDTO findProjectByIndex(final Integer Index) {
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest();
        request.setToken(token);
        request.setIndex(Index);
        return projectEndpoint.showProjectByIndex(request).getProjectDTO();
    }

    @Before
    public void before() {
        token = login(ADMIN_LOGIN, ADMIN_PASSWORD);
        saveBackup(token);
        testProjectDTO = createProject();
    }

    @After
    public void after() {
        loadBackup(token);
        logout(token);
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest();
        request.setToken(token);
        request.setId(testProjectDTO.getId());
        request.setStatus(Status.COMPLETED.toString());
        projectEndpoint.changeProjectStatusById(request);
        @Nullable ProjectDTO projectDTOFound = findProjectById(testProjectDTO.getId());
        Assert.assertEquals(projectDTOFound.getStatus(), Status.COMPLETED);
    }

    @Test
    public void changeProjectStatusByIndex() {
        @NotNull final Status statusBefore = findProjectByIndex(0).getStatus();
        @NotNull final Status statusToSet = statusBefore == Status.COMPLETED ? Status.NOT_STARTED : Status.COMPLETED;
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        request.setStatus(statusToSet.toString());
        projectEndpoint.changeProjectStatusByIndex(request);
        @Nullable ProjectDTO projectDTOFound = findProjectByIndex(0);
        Assert.assertEquals(projectDTOFound.getStatus(), statusToSet);
    }

    @Test
    public void clearProjects() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest();
        request.setToken(token);
        projectEndpoint.clearProjects(request);
        @NotNull final ProjectNotFoundException projectNotFoundException = new ProjectNotFoundException();
        thrown.expectMessage(projectNotFoundException.getMessage());
        Assert.assertNull(findProjectById(testProjectDTO.getId()));
    }

    @Test
    public void completeProjectById() {
        @NotNull final ProjectChangeStatusByIdRequest changeStatusRequest = new ProjectChangeStatusByIdRequest();
        changeStatusRequest.setToken(token);
        changeStatusRequest.setId(testProjectDTO.getId());
        changeStatusRequest.setStatus(Status.IN_PROGRESS.toString());
        projectEndpoint.changeProjectStatusById(changeStatusRequest);
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest();
        request.setToken(token);
        request.setId(testProjectDTO.getId());
        projectEndpoint.completeProjectById(request);
        @Nullable final ProjectDTO projectDTOFound = findProjectById(testProjectDTO.getId());
        Assert.assertEquals(projectDTOFound.getStatus(), Status.COMPLETED);
    }

    @Test
    public void projectCompleteByIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest changeStatusRequest = new ProjectChangeStatusByIndexRequest();
        changeStatusRequest.setToken(token);
        changeStatusRequest.setIndex(0);
        changeStatusRequest.setStatus(Status.IN_PROGRESS.toString());
        projectEndpoint.changeProjectStatusByIndex(changeStatusRequest);
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        projectEndpoint.completeProjectByIndex(request);
        @Nullable final ProjectDTO projectDTOFound = findProjectByIndex(0);
        Assert.assertEquals(projectDTOFound.getStatus(), Status.COMPLETED);
    }

    @Test
    public void listProjects() {
        @NotNull final ProjectListRequest request = new ProjectListRequest();
        request.setToken(token);
        request.setSort(Sort.BY_NAME.toString());
        @Nullable final List<ProjectDTO> projectDTOList = projectEndpoint.listProjects(request).getProjectDTOS();
        Assert.assertTrue(projectDTOList.size() >= 1);
    }

    @Test
    public void removeProjectById() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest();
        request.setToken(token);
        request.setId(testProjectDTO.getId());
        projectEndpoint.removeProjectById(request);
        @NotNull final ProjectNotFoundException projectNotFoundException = new ProjectNotFoundException();
        thrown.expectMessage(projectNotFoundException.getMessage());
        Assert.assertNull(findProjectById(testProjectDTO.getId()));
    }

    @Test
    public void removeProjectByIndex() {
        @NotNull final ProjectDTO projectDTOFound = findProjectByIndex(0);
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        projectEndpoint.removeProjectByIndex(request);
        Assert.assertNotEquals(projectDTOFound, findProjectByIndex(0));
    }

    @Test
    public void startProjectById() {
        @NotNull final ProjectChangeStatusByIdRequest changeStatusRequest = new ProjectChangeStatusByIdRequest();
        changeStatusRequest.setToken(token);
        changeStatusRequest.setId(testProjectDTO.getId());
        changeStatusRequest.setStatus(Status.NOT_STARTED.toString());
        projectEndpoint.changeProjectStatusById(changeStatusRequest);
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest();
        request.setToken(token);
        request.setId(testProjectDTO.getId());
        projectEndpoint.startProjectById(request);
        @Nullable final ProjectDTO projectDTOFound = findProjectById(testProjectDTO.getId());
        Assert.assertEquals(projectDTOFound.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startProjectByIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest changeStatusRequest = new ProjectChangeStatusByIndexRequest();
        changeStatusRequest.setToken(token);
        changeStatusRequest.setIndex(0);
        changeStatusRequest.setStatus(Status.NOT_STARTED.toString());
        projectEndpoint.changeProjectStatusByIndex(changeStatusRequest);
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        projectEndpoint.startProjectByIndex(request);
        @Nullable final ProjectDTO projectDTOFound = findProjectByIndex(0);
        Assert.assertEquals(projectDTOFound.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void updateProjectById() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest();
        request.setToken(token);
        request.setId(testProjectDTO.getId());
        request.setName(testProjectDTO.getName() + "1");
        request.setDescription(testProjectDTO.getDescription() + "1");
        projectEndpoint.updateProjectById(request);
        @Nullable final ProjectDTO projectDTOFound = findProjectById(testProjectDTO.getId());
        Assert.assertEquals(projectDTOFound.getName(), testProjectDTO.getName() + "1");
        Assert.assertEquals(projectDTOFound.getDescription(), testProjectDTO.getDescription() + "1");
    }

    @Test
    public void updateProjectByIndex() {
        @Nullable final ProjectDTO projectDTOToChange = findProjectByIndex(0);
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        request.setName(projectDTOToChange.getName() + "1");
        request.setDescription(projectDTOToChange.getDescription() + "1");
        projectEndpoint.updateProjectByIndex(request);
        @Nullable final ProjectDTO projectDTOFound = findProjectById(projectDTOToChange.getId());
        Assert.assertEquals(projectDTOFound.getName(), projectDTOToChange.getName() + "1");
        Assert.assertEquals(projectDTOFound.getDescription(), projectDTOToChange.getDescription() + "1");
    }

}

